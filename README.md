
**Wexam** is an opensource webapp allowing teacher to upload, associate and publish students' exam papers. Once published, students can access their papers and take time to review their mistakes.

It is built on top of the [Flask](https://flask.palletsprojects.com/en/2.2.x/) framework and uses Jinja templating to render HTML pages. Styling is based on the [Bulma](https://bulma.io) framework.

A [demo](https://demo.wexam.fr) with a fake authentication system is available to let you try it!

[[_TOC_]]

# Run

## Dependencies

Wexam uses `pdfinfo` and `pdftoppm` that must be in the `PATH`. These are provided by `poppler-utils`.

```bash
apt install poppler-utils
```

## Packaged version

Wexam is currently packaged in two ways:
- A simple Python package, hosted on the [repo's package registry](https://gitlab.com/wexam/wexam/-/packages).
- Docker images, hosted on the [repo's container registry](https://gitlab.com/wexam/wexam/container_registry).

### Python package

The Python packaged can be installed with:

```bash
pip install --upgrade wexam --index-url https://gitlab.com/api/v4/groups/55399059/-/packages/pypi/simple
```

This command will automatically install two commands in your `PATH`: 
- `wexam_dev_webserver` used for running the Flask web development server
- `wexam_page_insert_runner` used to start a runner associated to the webapp (that handles PDF manipulations)

**For production remark:**  the flask development server should not be run in production. For such use cases please use a WSGI server as `gunicorn`. For `gunicorn` use:

```bash
pip install gunicorn
gunicorn -b 0.0.0.0:5011 -t 600 -w 8 'wexam.app:create_app()'
```

### Docker images

If you want to use docker images, you can refer to the [`docker-compose.deploy.yaml`](./docker-compose.deploy.yaml) file that provides an example.

## Environment variables

Several environment variables need to be defined before running the app:

| Name | Description | Default value |
|------|-------------|---------------|
| WEXAM_POSTGRES_URL  | URL of the postgres DB, including user and password. | `postgresql://postgres:postgres@localhost:5432` |
| WEXAM_FLASK_SECRET_KEY  | Key used by the Flask server to secure communication | `9OLWxND4o83j4K4iuopO`|
| WEXAM_CAS_SERVER_URL | URL of the CAS server for authentication | |
| FAKE_CAS | Weither to mock a fake authentication (for demo purpose) | 0 |
| FAKE_DATA_URL | URL of the fake data to link when mocking a fake CAS | |
| SQL_ECHO | Output the SQL request to the console | 0 |


# Contribute

Any contribution to Wexam is welcome! If you find a bug or have an idea of a feature feel free to open an issue or to contact project maintainers to do so.

## Dependencies

Wexam uses `pdfinfo` and `pdftoppm` that must be in the `PATH`. These are provided by `poppler-utils`. Examples:
```bash
# Debian / Ubuntu
apt install poppler-utils
# macOS
brew install poppler
```

## Development set up

1. First, clone the repository.
1. *(Optional) You can use a virtual environment if you want to.* As an example, using `virtualenv`:
```bash
pip install virtualenv
cd wexam # Enter the repo where you cloned it
virtualenv -p python3 venv # Where 'venv' is the chosen name for the virtual env folder
source venv/bin/activate # This will activate the virtual env
```
3. Install development dependencies:
```bash
pip install -r requirements-dev.txt
pre-commit install # This will auto format your code and imports on commits
```
4. Then, install Wexam's dependencies:

```bash
pip install -r requirements.txt --index-url https://gitlab.com/api/v4/groups/55399059/-/packages/pypi/simple
pip install -e .
```

5. To start Wexam, you will need a running and accessible PostgreSQL database:
If you do not already have one, you can run it using the provided [`docker-compose.yaml`](./docker-compose.yaml):
```bash
docker-compose up --force-recreate
```
6. Define the environment variables you need and run wexam web server, i.e.:
```bash
WEXAM_POSTGRES_URL=postgresql://postgres:postgres@localhost:5432 \
FAKE_CAS=1 \
wexam_dev_webserver 
```

Wexam should now be running on port 5000 of your computer. Visit http://localhost:5000 to ensure everything is good! If so, you can now edit the code, Flask development server will automatically reload when changes are detected.

Enjoy :tada:!
