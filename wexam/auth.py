# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
from functools import wraps

import flask
from flask import session as flask_session, abort

from . import models
from .router import _helpers

if (load_fake_cas := os.getenv("FAKE_CAS")) is not None and load_fake_cas == "1":
    from .fake_auth import login
else:
    from flask_cas import login


def _override_login():
    overrided = os.getenv("LOGIN_OVERRIDE", None)
    if overrided is None:
        return
    login, role = overrided.split(",")
    flask_session["CAS_USERNAME"] = login
    flask_session["CAS_ATTRIBUTES"] = {
        "cas:sn": "Surname",
        "cas:givenName": "Name",
        "cas:ou": role,
    }


def login_required(function):
    @wraps(function)
    def wrap(*args, **kwargs):
        _override_login()  # for test purpose
        if "CAS_USERNAME" not in flask.session:
            flask.session["CAS_AFTER_LOGIN_SESSION_URL"] = (
                flask.request.script_root + flask.request.full_path
            )

            return login()
        else:
            with models.Session() as session:
                with session.begin():
                    user = (
                        session.query(models.User)
                        .filter(models.User.login == flask_session["CAS_USERNAME"])
                        .first()
                    )
                    if user is not None:
                        surname = flask_session["CAS_ATTRIBUTES"]["cas:sn"]
                        name = flask_session["CAS_ATTRIBUTES"]["cas:givenName"]
                        cas_role = (
                            models.Role.student
                            if flask_session["CAS_ATTRIBUTES"]["cas:ou"] == "students"
                            else models.Role.teacher
                        )
                        if user.surname != surname:
                            user.surname = surname
                        if user.name != name:
                            user.name = name
                        if user.cas_role != cas_role:
                            user.cas_role = cas_role
                    else:
                        user = models.User(
                            login=flask_session["CAS_USERNAME"],
                            surname=flask_session["CAS_ATTRIBUTES"]["cas:sn"],
                            name=flask_session["CAS_ATTRIBUTES"]["cas:givenName"],
                            cas_role=models.Role.student
                            if flask_session["CAS_ATTRIBUTES"]["cas:ou"] == "students"
                            else models.Role.teacher,
                        )
                        session.add(user)
                return function(*args, **kwargs, user=user, session=session)

    return wrap


def get_course_semester_and_membership(session, user, semester_code, course_code):
    course_semester = (
        session.query(models.CourseSemester)
        .join(models.Course)
        .join(models.Semester)
        .filter(models.Course.code == course_code)
        .filter(models.Semester.code == semester_code)
        .first()
    )
    if course_semester is None:
        abort(404)
    membership = (
        session.query(models.CourseMember)
        .filter(models.CourseMember.course_semester == course_semester)
        .filter(models.CourseMember.user == user)
        .first()
    )
    return course_semester, membership


def teacher_or_admin(session, user, semester_code, course_code):
    course_semester, membership = get_course_semester_and_membership(
        session, user, semester_code, course_code
    )
    if user.role != models.Role.admin:
        if membership is None or membership.role != models.Role.teacher:
            _helpers.log_course_semester_forbidden(session, user, course_semester)
            abort(403)
    return course_semester, membership


def teacher_or_admin_from_partid(session, user, part_id):
    part = session.query(models.ExamPart).filter(models.ExamPart.id == part_id).first()

    if part is None:
        abort(404)
    membership = (
        session.query(models.CourseMember)
        .filter(models.CourseMember.course_semester == part.exam.course_semester)
        .filter(models.CourseMember.user == user)
        .first()
    )
    if user.role != models.Role.admin:
        if membership is None or membership.role != models.Role.teacher:
            _helpers.log_part_forbidden(session, user, part)
            abort(403)
    return part, membership
