# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import re
import math
import unicodedata
from dataclasses import dataclass

from .. import models


@dataclass
class Student_result:
    grade: float
    displaygrade: str
    comment: str
    paper: str
    associated_paper: bool = False


def get_exam_results(
    session,
    exam,
    course_semester,
    prefix_by_exam=False,
    papers=False,
    details=True,
):

    exam_prefix = f"{exam.slug}_" if prefix_by_exam else ""

    exam_results = {
        f"{exam_prefix}{models.slugify(part.name)}": {
            part_paper.student.login: Student_result(
                grade=part_paper.grade,
                displaygrade=f"{part_paper.grade:.2f}"
                if part_paper.grade is not None
                else "",
                comment=part_paper.comment,
                paper=None,
                associated_paper=True,
            )
            for part_paper in part.part_papers
        }
        for part in exam.exam_parts
    }

    course_members = (
        session.query(models.CourseMember)
        .filter(models.CourseMember.course_semester == course_semester)
        .all()
    )

    students = [  # not possible to do easily in db, because role is a property
        cm for cm in course_members if cm.role == models.Role.student
    ]

    for key, val in exam_results.items():
        for student in students:
            if student.login not in val:
                val[student.login] = Student_result(
                    grade=None,
                    displaygrade="",
                    comment=None,
                    paper=None,
                    associated_paper=False,
                )

    paper_prefix = f"/render/pdf/{course_semester.semester.code}_{course_semester.course.code}_{exam.slug}"

    if len(exam.exam_parts) == 1:
        key, part_results = next(exam_results.items().__iter__())
        exam_results[f"{exam_prefix}Somme"] = {
            login: Student_result(
                grade=res.grade,
                displaygrade=res.displaygrade,
                comment=res.comment,
                paper=None,
            )
            for login, res in part_results.items()
        }
        if papers:
            exam_results[f"{exam_prefix}Copie"] = {
                login: Student_result(
                    grade=None,
                    displaygrade="",
                    comment=None,
                    paper=(
                        f"{paper_prefix}_{login}.pdf" if res.associated_paper else None
                    ),
                )
                for login, res in part_results.items()
            }
        del exam_results[key]
    else:
        sum_column = {
            student.login: Student_result(
                grade=(
                    g := (
                        None
                        if all(
                            part_res[student.login].grade is None
                            for part_res in exam_results.values()
                        )
                        else sum(
                            part_res[student.login].grade
                            for part_res in exam_results.values()
                            if part_res[student.login].grade is not None
                        )
                    )
                ),
                displaygrade=f"{g:.2f}" if g is not None else "",
                paper=(
                    f"{paper_prefix}_{student.login}.pdf"
                    if any(
                        part_res[student.login].associated_paper
                        for part_res in exam_results.values()
                    )
                    else None
                ),
                comment=None,
            )
            for student in students
        }
        if not details:
            exam_results = {}
        exam_results[f"{exam_prefix}Somme"] = {
            login: Student_result(
                grade=res.grade,
                displaygrade=res.displaygrade,
                paper=None,
                comment=res.comment,
            )
            for login, res in sum_column.items()
        }
        if papers:
            exam_results[f"{exam_prefix}Copie"] = {
                login: Student_result(
                    grade=None,
                    displaygrade="",
                    paper=res.paper,
                    comment=None,
                )
                for login, res in sum_column.items()
            }

    return students, exam_results


def sort_key_literal(x):
    return re.sub("[^a-z]", "", unicodedata.normalize("NFKD", x).lower())


def sort_key_number(x):
    return -(-math.inf if x is None else x)


def argsort(x, key):
    return [i for i, _ in sorted(enumerate(x), key=lambda y: key(y[1]))]


def _revperm(p):
    return [i for i, _ in sorted(enumerate(p), key=lambda w: w[1])]


def revargsort(x, key):
    return _revperm(argsort(x, key))
