# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import re
import unicodedata
import dateutil.parser
import datetime

from flask import request

from .. import models

STATUS_FRONT = {
    models.AssociationStatus.nopages: {
        "status": "Aucune copie",
        "class": "is-danger",
        "describe": "Aucune copie n'a été chargée.",
    },
    models.AssociationStatus.nonassociated: {
        "status": "Non associé",
        "class": "is-warning",
        "describe": "Aucune copie n'a été associée.",
    },
    models.AssociationStatus.partially_associated: {
        "status": "Partiellement associé",
        "class": "is-info",
        "describe": "Certaines copies ont été associées, mais certaines ne le sont pas encore.",
    },
    models.AssociationStatus.associated: {
        "status": "Associé",
        "class": "is-success",
        "describe": "Toutes les copies chargées ont été associées.",
    },
    models.PublicationStatus.unpublished: {
        "status": "Non publié",
        "class": "is-warning",
        "describe": "Aucune copie n'a été publiée.",
    },
    models.PublicationStatus.partially_published: {
        "status": "Partiellement publié",
        "class": "is-info",
        "describe": "Certaines copies associées n'ont pas encore été publiées.",
    },
    models.PublicationStatus.published: {
        "status": "Publié",
        "class": "is-success",
        "describe": "Toutes les copies asociées ont été publiées.",
    },
}


def parsedate(x):
    x = re.sub(
        "[^a-z0-9/-]",
        "",
        unicodedata.normalize("NFKD", x)
        .encode("ascii", errors="ignore")
        .decode("ascii")
        .lower(),
    )
    for french, english in [
        ("janvier", "january"),
        ("fevrier", "february"),
        ("mars", "march"),
        ("avril", "april"),
        ("mai", "may"),
        ("juin", "june"),
        ("juillet", "july"),
        ("aout", "august"),
        ("septembre", "september"),
        ("octobre", "october"),
        ("novembre", "november"),
        ("decembre", "december"),
    ]:
        x = x.replace(french, english)
    return dateutil.parser.parse(x)


def normalize_str(x):
    if x is not None:
        return re.sub(
            "[^a-z0-9]",
            "",
            unicodedata.normalize("NFKD", x)
            .encode("ascii", errors="ignore")
            .decode("ascii")
            .lower(),
        )
    return ""


def capitalize_str(x):
    if x is None:
        return None
    return " ".join(
        "-".join("'".join(x.capitalize() for x in x.split("'")) for x in x.split("-"))
        for x in x.split(" ")
    )


def log_user_action(session, user, action, target_user=None):
    log = models.Log(
        datetime=datetime.datetime.now(),
        origin_user=user,
        impacted_user=target_user,
        action=action,
    )
    session.add(log)


def log_course_semester_action(
    session, user, course_semester, action, target_user=None
):
    log = models.Log(
        datetime=datetime.datetime.now(),
        origin_user=user,
        impacted_course=course_semester.course,
        impacted_semester=course_semester.semester,
        impacted_course_semester=course_semester,
        impacted_user=target_user,
        action=action,
    )
    session.add(log)


def log_exam_action(session, user, exam, action):
    log = models.Log(
        datetime=datetime.datetime.now(),
        origin_user=user,
        impacted_course=exam.course_semester.course,
        impacted_semester=exam.course_semester.semester,
        impacted_course_semester=exam.course_semester,
        impacted_exam=exam,
        action=action,
    )
    session.add(log)


def log_part_action(session, user, part, action):
    log = models.Log(
        datetime=datetime.datetime.now(),
        origin_user=user,
        impacted_course=part.exam.course_semester.course,
        impacted_semester=part.exam.course_semester.semester,
        impacted_course_semester=part.exam.course_semester,
        impacted_exam=part.exam,
        impacted_exam_part=part,
        action=action,
    )
    session.add(log)


def log_insertionjob_action(session, user, insertion_job, action):
    log = models.Log(
        datetime=datetime.datetime.now(),
        origin_user=user,
        impacted_course=insertion_job.exam_part.exam.course_semester.course,
        impacted_semester=insertion_job.exam_part.exam.course_semester.semester,
        impacted_course_semester=insertion_job.exam_part.exam.course_semester,
        impacted_exam=insertion_job.exam_part.exam,
        impacted_exam_part=insertion_job.exam_part,
        action=action,
    )
    session.add(log)


def log_gp_action(session, user, gp, action, target_user=None):
    log = models.Log(
        datetime=datetime.datetime.now(),
        origin_user=user,
        impacted_user=target_user,
        impacted_course=gp.exam_part.exam.course_semester.course,
        impacted_semester=gp.exam_part.exam.course_semester.semester,
        impacted_course_semester=gp.exam_part.exam.course_semester,
        impacted_exam=gp.exam_part.exam,
        impacted_exam_part=gp.exam_part,
        impacted_group_of_pages=gp,
        action=action,
    )
    session.add(log)


def log_partpaper_action(session, user, part_paper, action, target_user=None):
    log = models.Log(
        datetime=datetime.datetime.now(),
        origin_user=user,
        impacted_user=target_user,
        impacted_course=part_paper.exam_part.exam.course_semester.course,
        impacted_semester=part_paper.exam_part.exam.course_semester.semester,
        impacted_course_semester=part_paper.exam_part.exam.course_semester,
        impacted_exam=part_paper.exam_part.exam,
        impacted_exam_part=part_paper.exam_part,
        impacted_part_paper=part_paper,
        action=action,
    )
    session.add(log)


def log_course_semester_forbidden(session, user, course_semester):
    log = models.Log(
        datetime=datetime.datetime.now(),
        origin_user=user,
        impacted_course=course_semester.course,
        impacted_semester=course_semester.semester,
        impacted_course_semester=course_semester,
        action=f"Forbidden {request.path}",
    )
    session.add(log)
    session.commit()


def log_part_forbidden(session, user, part):
    log = models.Log(
        datetime=datetime.datetime.now(),
        origin_user=user,
        impacted_course=part.exam.course_semester.course,
        impacted_semester=part.exam.course_semester.semester,
        impacted_course_semester=part.exam.course_semester,
        impacted_exam=part.exam,
        impacted_exam_part=part,
        action=f"Forbidden {request.path}",
    )
    session.add(log)
    session.commit()


def log_forbidden(session, user):
    log = models.Log(
        datetime=datetime.datetime.now(),
        origin_user=user,
        action=f"Forbidden {request.path}",
    )
    session.add(log)
    session.commit()
