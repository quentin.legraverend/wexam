# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import io
import csv
from dataclasses import dataclass

import cchardet
from flask import Blueprint, render_template, redirect, url_for, request, abort

from .. import auth
from .. import models
from . import _helpers
from . import _exam_helpers

course = Blueprint(
    "course",
    __name__,
    url_prefix="/course",
    template_folder="templates",
    static_folder="static",
)


@dataclass
class ZipFile:
    url: str
    name: str


@course.route("/<semester_code>/<course_code>/table", methods=["GET"])
@auth.login_required
def table_exams(semester_code, course_code, user=None, session=None):
    details = True if request.args.get("details") == "true" else False
    with session.begin():
        course_semester, _ = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )

        exams = (
            session.query(models.Exam)
            .filter(models.Exam.course_semester == course_semester)
            .order_by(models.Exam.date)
            .all()
        )
        if not exams:
            abort(404)

        _helpers.log_course_semester_action(
            session, user, course_semester, "View course table"
        )
        exams_results = {}
        for exam in exams:
            students, exam_results = _exam_helpers.get_exam_results(
                session,
                exam,
                course_semester,
                papers=True,
                prefix_by_exam=True,
                details=details,
            )
            exams_results.update(exam_results)

        columns = tuple(exams_results.keys())

        zipfiles = [
            ZipFile(
                url=f"/render/pdfzip/{semester_code}_{course_code}_{exam.slug}.zip",
                name=f"{exam.name}",
            )
            for exam in exams
        ]

        students.sort(
            key=lambda x: (
                _exam_helpers.sort_key_literal(x.surname),
                _exam_helpers.sort_key_literal(x.name),
                x.login,
            ),
        )

        orders = {
            "Nom": _exam_helpers.revargsort(
                students,
                key=lambda x: (
                    _exam_helpers.sort_key_literal(x.surname),
                    _exam_helpers.sort_key_literal(x.name),
                    x.login,
                ),
            ),
            "Prénom": _exam_helpers.revargsort(
                students,
                key=lambda x: (
                    _exam_helpers.sort_key_literal(x.name),
                    _exam_helpers.sort_key_literal(x.surname),
                    x.login,
                ),
            ),
        }

        for col in columns:
            orders[col] = _exam_helpers.revargsort(
                students,
                key=lambda x: (
                    _exam_helpers.sort_key_number(exams_results[col][x.login].grade),
                    x.login,
                ),
            )
    return render_template(
        "table.html",
        user=user,
        students=students,
        course=course_semester,
        columns=columns,
        exam_results=exams_results,
        tablebasename=f"{semester_code}_{course_code}",
        zips=zipfiles,
        orders=orders,
        details=details,
    )


@course.route("/<semester_code>/<course_code>/members", methods=["GET"])
@auth.login_required
def course_members(semester_code, course_code, user=None, session=None):
    with session.begin():
        course_semester, membership = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )
        _helpers.log_course_semester_action(
            session, user, course_semester, "View course members"
        )
        students = [u for u in course_semester.users if u.role == models.Role.student]
        students.sort(
            key=lambda m: (
                _helpers.normalize_str(m.surname),
                _helpers.normalize_str(m.name),
                m.birthdate,
            )
        )
        teachers = [u for u in course_semester.users if u.role == models.Role.teacher]
        teachers.sort(
            key=lambda m: (
                _helpers.normalize_str(m.surname),
                _helpers.normalize_str(m.name),
                m.birthdate,
            )
        )
        return render_template(
            "members.html",
            user=user,
            course_semester=course_semester,
            membership=membership,
            students=students,
            teachers=teachers,
        )


@course.route(
    "/<semester_code>/<course_code>/members/delete/<impacted_login>", methods=["GET"]
)
@auth.login_required
def course_member_delete(
    semester_code, course_code, impacted_login, user=None, session=None
):
    with session.begin():
        course_semester, _ = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )
        if impacted_login != user.login:
            impacted_user = (
                session.query(models.User)
                .filter(models.User.login == impacted_login)
                .first()
            )
            if impacted_user is not None:
                _helpers.log_course_semester_action(
                    session, user, course_semester, "Delete subscription", impacted_user
                )
                part_papers = (
                    session.query(models.PartPaper)
                    .filter(models.PartPaper.student == impacted_user)
                    .all()
                )
                for p in part_papers:
                    for g in p.groups_of_pages:
                        g.part_paper_id = None
                session.query(models.PartPaper).filter(
                    models.PartPaper.student == impacted_user
                ).delete()

                session.query(models.CourseMember).filter(
                    models.CourseMember.user == impacted_user
                ).filter(
                    models.CourseMember.course_semester == course_semester
                ).delete()
    return redirect(
        url_for(
            "course.course_members",
            semester_code=semester_code,
            course_code=course_code,
        )
    )


def _add_user_to_course(
    session,
    user,
    course_semester,
    impacted_login,
    role=models.Role.student,
    surname=None,
    name=None,
    birthdate=None,
):
    impacted_user = (
        session.query(models.User).filter(models.User.login == impacted_login).first()
    )
    if impacted_user is None:
        impacted_user = models.User(login=impacted_login)
    else:
        if (
            session.query(models.CourseMember)
            .filter(models.CourseMember.course_semester == course_semester)
            .filter(models.CourseMember.user == impacted_user)
            .count()
            > 0
        ):
            return None
    course_member = models.CourseMember(
        user=impacted_user,
        course_semester=course_semester,
        local_role=role,
        local_name=_helpers.capitalize_str(name),
        local_surname=_helpers.capitalize_str(surname),
        local_birthdate=birthdate,
    )
    session.add(course_member)
    _helpers.log_course_semester_action(
        session, user, course_semester, "Add subscribtion", impacted_user
    )


@course.route("/<semester_code>/<course_code>/members/add/teacher", methods=["POST"])
@auth.login_required
def course_member_add_teacher(semester_code, course_code, user=None, session=None):
    with session.begin():
        course_semester, _ = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )
        impacted_login = request.form["login"]
        _add_user_to_course(
            session, user, course_semester, impacted_login, role=models.Role.teacher
        )

    return redirect(
        url_for(
            "course.course_members",
            semester_code=semester_code,
            course_code=course_code,
        )
    )


@course.route("/<semester_code>/<course_code>/members/add/student", methods=["POST"])
@auth.login_required
def course_member_add_student(semester_code, course_code, user=None, session=None):
    with session.begin():
        course_semester, _ = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )
        impacted_login = request.form["login"]
        name = request.form["name"]
        surname = request.form["surname"]
        birthdate = request.form["birthdate"]
        _add_user_to_course(
            session,
            user,
            course_semester,
            impacted_login,
            role=models.Role.student,
            name=name,
            surname=surname,
            birthdate=birthdate,
        )

    return redirect(
        url_for(
            "course.course_members",
            semester_code=semester_code,
            course_code=course_code,
        )
    )


class _GetColnames:
    def __init__(self, fieldnames):
        self._normalized_fieldnames = {
            _helpers.normalize_str(field): field for field in fieldnames
        }

    def __call__(self, alternatives):
        for x in alternatives:
            xn = _helpers.normalize_str(x)
            if xn in self._normalized_fieldnames:
                return self._normalized_fieldnames[xn]


@course.route("/<semester_code>/<course_code>/members/add/list", methods=["POST"])
@auth.login_required
def course_member_add_list(semester_code, course_code, user=None, session=None):
    csvcontent = request.files["list"].read()
    with session.begin():
        course_semester, _ = auth.teacher_or_admin(
            session, user, semester_code, course_code
        )

        encoding = cchardet.detect(csvcontent)["encoding"]
        if encoding is None:
            abort(415)
        csvdecoded = csvcontent.decode(encoding)
        dialect = csv.Sniffer().sniff(csvdecoded)
        csvreader = csv.DictReader(io.StringIO(csvdecoded), dialect=dialect)
        colnames_get = _GetColnames(csvreader.fieldnames)
        logincol = colnames_get(("login",))
        surnamecol = colnames_get(("nom", "name", "surname"))
        namecol = colnames_get(("prenom", "given name", "first name"))
        birthdatecol = colnames_get(("date de naissance", "birth date"))
        if any(x is None for x in (logincol, surnamecol, namecol, birthdatecol)):
            abort(415)
        for row in csvreader:
            _add_user_to_course(
                session,
                user,
                course_semester,
                row[logincol],
                role=models.Role.student,
                name=row[namecol],
                surname=row[surnamecol],
                birthdate=_helpers.parsedate(row[birthdatecol]),
            )

    return redirect(
        url_for(
            "course.course_members",
            semester_code=semester_code,
            course_code=course_code,
        )
    )
