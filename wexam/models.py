# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import re
import enum
import unicodedata

from sqlalchemy import create_engine
from sqlalchemy import (
    Boolean,
    Column,
    Date,
    DateTime,
    Enum,
    Float,
    ForeignKey,
    ForeignKeyConstraint,
    Index,
    Integer,
    LargeBinary,
    UniqueConstraint,
    Text,
)
from sqlalchemy.exc import OperationalError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, deferred
from sqlalchemy_utils import ChoiceType, URLType

slugify = lambda string: re.sub(
    r"[-\s]+",
    "-",
    re.sub(
        r"[^a-z0-9]",
        "-",
        unicodedata.normalize("NFKD", string)
        .encode("ascii", "ignore")
        .decode()
        .lower(),
    ).strip(),
)

db_string = os.getenv(
    "WEXAM_POSTGRES_URL", "postgresql://postgres:postgres@localhost:5432"
)
echo = os.getenv("SQL_ECHO", "0")

db = create_engine(db_string, echo=(echo == "1"), isolation_level="REPEATABLE READ")
db_serializable = db.execution_options(isolation_level="SERIALIZABLE")

Session = sessionmaker(db)
base = declarative_base()


class Role(enum.Enum):
    admin = 0
    teacher = 1
    student = 2


class User(base):
    __tablename__ = "users"
    login = Column(Text, primary_key=True)
    cas_role = Column(Enum(Role))
    override_role = Column(Enum(Role))
    name = Column(Text)
    surname = Column(Text)
    birthdate = Column(Date)

    courses_memberships = relationship("CourseMember", back_populates="user")

    @property
    def role(self):
        if self.override_role:
            return self.override_role
        return self.cas_role


class Course(base):
    __tablename__ = "courses"
    code = Column(Text, primary_key=True)
    name = Column(Text)

    courses_semesters = relationship("CourseSemester", back_populates="course")


class Semester(base):
    __tablename__ = "semesters"
    code = Column(Text, primary_key=True)
    name = Column(Text, nullable=False)
    start_date = Column(Date)
    end_date = Column(Date)

    courses_semesters = relationship("CourseSemester", back_populates="semester")


class CourseSemester(base):
    __tablename__ = "courses_semesters"
    course_code = Column(Text, ForeignKey("courses.code"), primary_key=True, index=True)
    semester_code = Column(
        Text, ForeignKey("semesters.code"), primary_key=True, index=True
    )
    closed = Column(Boolean)

    _number_of_students = None

    course = relationship("Course", back_populates="courses_semesters")
    semester = relationship("Semester", back_populates="courses_semesters")
    exams = relationship(
        "Exam", back_populates="course_semester", order_by="Exam.date.asc()"
    )
    users = relationship("CourseMember", back_populates="course_semester")

    @property
    def number_of_students(self):
        if self._number_of_students is not None:
            return self._number_of_students

        students = [student for student in self.users if student.role == Role.student]
        self._number_of_students = len(students)
        return self._number_of_students


class PublicationStatus(enum.Enum):
    unpublished = 0
    partially_published = 1
    published = 2


class AssociationStatus(enum.Enum):
    nopages = 0
    nonassociated = 1
    partially_associated = 2
    associated = 3


class Exam(base):
    __tablename__ = "exams"
    course_code = Column(Text, primary_key=True)
    semester_code = Column(Text, primary_key=True)
    slug = Column(Text, primary_key=True)
    name = Column(Text, nullable=False)
    correction = Column(URLType)  # Most likely a link to a correction for now...
    date = Column(Date)

    _status = None

    __table_args__ = (
        ForeignKeyConstraint(
            (
                "course_code",
                "semester_code",
            ),
            ("courses_semesters.course_code", "courses_semesters.semester_code"),
        ),
        Index(
            "ix_exams_course_semester",
            course_code,
            semester_code,
        ),
    )

    course_semester = relationship("CourseSemester", back_populates="exams")
    exam_parts = relationship(
        "ExamPart", back_populates="exam", order_by="ExamPart.position_in_exam.asc()"
    )

    def has_part_paper_for_student(self, user):
        return any(
            part.part_paper_of_student(user) is not None for part in self.exam_parts
        )

    @property
    def status(self):
        if self._status is not None:
            return self._status

        parts_status = [part.status for part in self.exam_parts]
        parts_assoc_status = [s[0] for s in parts_status]
        parts_publi_status = [s[1] for s in parts_status]

        pages_status, publish_status = None, None
        if all(s == AssociationStatus.nopages for s in parts_assoc_status):
            pages_status = AssociationStatus.nopages
        elif all(
            s in (AssociationStatus.nopages, AssociationStatus.nonassociated)
            for s in parts_assoc_status
        ):
            pages_status = AssociationStatus.nonassociated
        else:
            if all(
                (s in (AssociationStatus.associated, AssociationStatus.nopages))
                for s in parts_assoc_status
            ):
                pages_status = AssociationStatus.associated
            else:
                pages_status = AssociationStatus.partially_associated
            if all(
                (s2 == PublicationStatus.published or s1 == AssociationStatus.nopages)
                for s1, s2 in parts_status
            ):
                publish_status = PublicationStatus.published
            elif not all(
                (s2 == PublicationStatus.unpublished or s1 == AssociationStatus.nopages)
                for s1, s2 in parts_status
            ):
                publish_status = PublicationStatus.partially_published
            else:
                publish_status = PublicationStatus.unpublished

        self._status = (pages_status, publish_status)
        return self._status


class ExamPart(base):
    __tablename__ = "exam_parts"
    id = Column(Integer, primary_key=True)
    course_code = Column(Text, nullable=False)
    semester_code = Column(Text, nullable=False)
    exam_slug = Column(Text, nullable=False)
    name = Column(Text)
    position_in_exam = Column(Integer)

    _status = None

    __table_args__ = (
        ForeignKeyConstraint(
            (
                "course_code",
                "semester_code",
                "exam_slug",
            ),
            ("exams.course_code", "exams.semester_code", "exams.slug"),
        ),
        Index(
            "ix_exam_parts_exam",
            course_code,
            semester_code,
            exam_slug,
        ),
    )

    exam = relationship("Exam", back_populates="exam_parts")
    groups_of_pages = relationship("GroupOfPages", back_populates="exam_part")
    part_papers = relationship("PartPaper", back_populates="exam_part")
    insertion_jobs = relationship(
        "InsertionJob", back_populates="exam_part", order_by="InsertionJob.id.asc()"
    )

    def part_paper_of_student(self, user):
        try:
            return next(
                pp
                for pp in self.part_papers
                if pp.published and pp.student_login == user.login
            )
        except StopIteration:
            pass
        return None

    @property
    def status_insertion_waiting_or_running(self):
        return any(
            job.status
            in (
                InsertionStatus.waiting,
                InsertionStatus.starting,
                InsertionStatus.running,
            )
            for job in self.insertion_jobs
        )

    @property
    def nb_groups(self):
        return len(self.groups_of_pages)

    @property
    def nb_groups_associated(self):
        return sum(gp.part_paper_id is not None for gp in self.groups_of_pages)

    @property
    def status(self):
        if self._status is not None:
            return self._status

        pages_status, publish_status = None, None
        if not self.groups_of_pages:
            pages_status = AssociationStatus.nopages
        elif all(gp.part_paper_id is None for gp in self.groups_of_pages):
            pages_status = AssociationStatus.nonassociated
            publish_status = PublicationStatus.unpublished
        else:
            if all(gp.part_paper_id is not None for gp in self.groups_of_pages):
                pages_status = AssociationStatus.associated
            else:
                pages_status = AssociationStatus.partially_associated
            if all(paper.published for paper in self.part_papers):
                publish_status = PublicationStatus.published
            elif any(paper.published for paper in self.part_papers):
                publish_status = PublicationStatus.partially_published
            else:
                publish_status = PublicationStatus.unpublished

        self._status = (pages_status, publish_status)
        return self._status

    @property
    def published(self):
        return any(pp.published for pp in self.part_papers)

    @property
    def number_of_published(self):
        return len([pp for pp in self.part_papers if pp.published == True])

    @property
    def number_of_graded_students(self):
        return sum(pp.grade is not None for pp in self.part_papers)

    @property
    def number_of_associated_students(self):
        return len([pp.student_login for pp in self.part_papers])


class GroupOfPages(base):
    __tablename__ = "groups_of_pages"
    id = Column(Integer, primary_key=True)
    exam_part_id = Column(
        Integer, ForeignKey("exam_parts.id"), nullable=False, index=True
    )
    part_paper_id = Column(Integer, ForeignKey("part_papers.id"), index=True)
    position_in_part = Column(Integer)
    format = Column(Integer)

    exam_part = relationship("ExamPart", back_populates="groups_of_pages")
    part_paper = relationship("PartPaper", back_populates="groups_of_pages")
    pages = relationship(
        "Page", back_populates="group_of_pages", order_by="Page.position_in_group.asc()"
    )


class Page(base):
    ORIENTATIONS = (
        ("0", "0"),
        ("90", "90"),
        ("180", "180"),
        ("270", "270"),
    )
    __tablename__ = "pages"
    id = Column(Integer, primary_key=True)
    orientation = Column(ChoiceType(ORIENTATIONS))
    content = deferred(Column(LargeBinary))
    position_in_group = Column(Integer)
    group_id = Column(
        Integer, ForeignKey("groups_of_pages.id"), nullable=False, index=True
    )

    group_of_pages = relationship("GroupOfPages", back_populates="pages")


class PartPaper(base):
    __tablename__ = "part_papers"
    id = Column(Integer, primary_key=True)
    student_login = Column(Text, ForeignKey("users.login"), nullable=False, index=True)
    grade = Column(Float)
    comment = Column(Text)
    published = Column(Boolean)
    exam_part_id = Column(
        Integer, ForeignKey("exam_parts.id"), nullable=False, index=True
    )

    __table_args__ = (UniqueConstraint("exam_part_id", "student_login"),)

    exam_part = relationship("ExamPart", back_populates="part_papers")
    student = relationship("User")
    groups_of_pages = relationship(
        "GroupOfPages",
        back_populates="part_paper",
        order_by="GroupOfPages.position_in_part.asc()",
    )

    @property
    def displaygrade(self):
        if self.grade is not None:
            return f"{self.grade:.2f}"
        return self.grade


class CourseMember(base):
    __tablename__ = "courses_members"
    login = Column(
        Text, ForeignKey("users.login"), primary_key=True, nullable=False, index=True
    )
    course_code = Column(Text, primary_key=True)
    semester_code = Column(Text, primary_key=True)
    local_role = Column(Enum(Role))  # override the role of the user only for the course

    __table_args__ = (
        ForeignKeyConstraint(
            (
                "course_code",
                "semester_code",
            ),
            ("courses_semesters.course_code", "courses_semesters.semester_code"),
        ),
        Index(
            "ix_courses_members_course_semester",
            course_code,
            semester_code,
        ),
    )

    # Those will be used until the user has connected with the CAS
    local_name = Column(Text)
    local_surname = Column(Text)
    local_birthdate = Column(Date)

    user = relationship("User", back_populates="courses_memberships")
    course_semester = relationship("CourseSemester", back_populates="users")

    @property
    def name(self):
        if self.user.name is not None:  # global have precedence
            return self.user.name
        return self.local_name

    @property
    def surname(self):
        if self.user.surname is not None:  # global have precedence
            return self.user.surname
        return self.local_surname

    @property
    def birthdate(self):
        if self.user.birthdate is not None:  # global have precedence
            return self.user.birthdate
        return self.local_birthdate

    @property
    def role(self):
        if self.local_role is not None:  # local have precedence
            return self.local_role
        return self.user.role


class InsertionStatus(enum.Enum):
    waiting = 0
    starting = 1
    running = 2
    done = 3
    error = 4

    @property
    def display(self):
        if self == InsertionStatus.waiting:
            return "En attente"
        if self == InsertionStatus.starting:
            return "Initialisation"
        if self == InsertionStatus.running:
            return "En cours"
        if self == InsertionStatus.done:
            return "Terminé"
        if self == InsertionStatus.error:
            return "Erreur"


class InsertionJob(base):
    __tablename__ = "insertion_jobs"
    id = Column(Integer, primary_key=True)
    exam_part_id = Column(
        Integer, ForeignKey("exam_parts.id"), nullable=False, index=True
    )
    status = Column(Enum(InsertionStatus), default=InsertionStatus.waiting, index=True)
    format = Column(Integer, nullable=False)
    errormsg = Column(Text)
    no_pages_total = Column(Integer)
    no_pages_inserted = Column(Integer)
    datetime = Column(DateTime)
    filename = Column(Text)

    exam_part = relationship("ExamPart", back_populates="insertion_jobs")
    job_file = relationship("InsertionJobFile", back_populates="job")


class InsertionJobFile(base):
    __tablename__ = "insertion_jobs_files"
    id = Column(
        Integer, ForeignKey("insertion_jobs.id"), nullable=False, primary_key=True
    )
    content = deferred(Column(LargeBinary))

    job = relationship("InsertionJob", back_populates="job_file")


class Log(base):
    __tablename__ = "logs"
    id = Column(Integer, primary_key=True)
    datetime = Column(DateTime, nullable=False)
    origin_login = Column(Text, ForeignKey("users.login"), nullable=False, index=True)

    action = Column(Text)

    impacted_user_login = Column(Text, ForeignKey("users.login"), index=True)
    impacted_course_code = Column(
        Text, ForeignKey("courses.code", ondelete="SET NULL"), index=True
    )
    impacted_semester_code = Column(
        Text, ForeignKey("semesters.code", ondelete="SET NULL"), index=True
    )
    impacted_course_semester_course_code = Column(Text)
    impacted_course_semester_semester_code = Column(Text)
    impacted_exam_course_code = Column(Text)
    impacted_exam_semester_code = Column(Text)
    impacted_exam_exam_slug = Column(Text)
    impacted_exam_part_id = Column(
        Integer, ForeignKey("exam_parts.id", ondelete="SET NULL"), index=True
    )
    impacted_group_of_pages_id = Column(
        Integer, ForeignKey("groups_of_pages.id", ondelete="SET NULL"), index=True
    )
    impacted_page_id = Column(
        Integer, ForeignKey("pages.id", ondelete="SET NULL"), index=True
    )
    impacted_part_paper_id = Column(
        Integer, ForeignKey("part_papers.id", ondelete="SET NULL"), index=True
    )
    impacted_course_member_login = Column(Text)
    impacted_course_member_course_code = Column(Text)
    impacted_course_member_semester_code = Column(Text)
    impacted_insertion_job_id = Column(
        Integer, ForeignKey("insertion_jobs.id", ondelete="SET NULL"), index=True
    )

    __table_args__ = (
        ForeignKeyConstraint(
            (
                "impacted_course_semester_course_code",
                "impacted_course_semester_semester_code",
            ),
            ("courses_semesters.course_code", "courses_semesters.semester_code"),
            ondelete="SET NULL",
        ),
        Index(
            "ix_logs_impacted_course_semester",
            impacted_course_semester_course_code,
            impacted_course_semester_semester_code,
        ),
        ForeignKeyConstraint(
            (
                "impacted_course_member_login",
                "impacted_course_member_course_code",
                "impacted_course_member_semester_code",
            ),
            (
                "courses_members.login",
                "courses_members.course_code",
                "courses_members.semester_code",
            ),
            ondelete="SET NULL",
        ),
        Index(
            "ix_logs_impacted_course_member",
            impacted_course_member_login,
            impacted_course_member_course_code,
            impacted_course_member_semester_code,
        ),
        ForeignKeyConstraint(
            (
                "impacted_exam_course_code",
                "impacted_exam_semester_code",
                "impacted_exam_exam_slug",
            ),
            (
                "exams.course_code",
                "exams.semester_code",
                "exams.slug",
            ),
            ondelete="SET NULL",
        ),
        Index(
            "ix_logs_impacted_exam",
            impacted_course_member_login,
            impacted_course_member_course_code,
            impacted_course_member_semester_code,
        ),
    )

    origin_user = relationship("User", foreign_keys=(origin_login,))
    impacted_user = relationship("User", foreign_keys=(impacted_user_login,))
    impacted_course = relationship("Course")
    impacted_semester = relationship("Semester")
    impacted_course_semester = relationship("CourseSemester")
    impacted_exam = relationship("Exam")
    impacted_exam_part = relationship("ExamPart")
    impacted_group_of_pages = relationship("GroupOfPages")
    impacted_page = relationship("Page")
    impacted_part_paper = relationship("PartPaper")
    impacted_course_member = relationship("CourseMember")
    impacted_insertion_job = relationship("InsertionJob")
