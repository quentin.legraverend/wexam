# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from flask import current_app, redirect, session as flask_session, url_for

from .. import models


def fake_login(form=None, role="student"):
    from flask_cas.cas_urls import create_cas_login_url

    if form is None:
        redirect_url = create_cas_login_url(
            "", "/cas/login", flask_session.get("CAS_AFTER_LOGIN_SESSION_URL")
        )

        return redirect(redirect_url)

    else:
        if fake_validate(form, role):
            return redirect(url_for("main.view"))

        else:
            redirect_url = create_cas_login_url(
                "", "/cas/login", flask_session.get("CAS_AFTER_LOGIN_SESSION_URL")
            )
            return redirect(redirect_url)


def fake_logout(session=None):
    cas_username_session_key = current_app.config["CAS_USERNAME_SESSION_KEY"]
    cas_attributes_session_key = current_app.config["CAS_ATTRIBUTES_SESSION_KEY"]

    if cas_username_session_key in flask_session:
        del session[cas_username_session_key]

    if cas_attributes_session_key in flask_session:
        del session[cas_attributes_session_key]


def fake_validate(form, role):
    # We should check here that member is in a file...
    cas_username_session_key = current_app.config["CAS_USERNAME_SESSION_KEY"]
    cas_attributes_session_key = current_app.config["CAS_ATTRIBUTES_SESSION_KEY"]

    if role == "teacher":
        login = form["login"]

        attributes = {
            "cas:sn": form["surname"],
            "cas:givenName": form["name"],
            "cas:ou": role,
        }

    elif role == "students":
        with models.Session() as session:
            student = (
                session.query(models.User)
                .filter(models.User.login == form["student"])
                .first()
            )

            login = student.login

            attributes = {
                "cas:sn": student.surname,
                "cas:givenName": student.name,
                "cas:ou": "students",
            }

    flask_session[cas_username_session_key] = login
    flask_session[cas_attributes_session_key] = attributes

    print(flask_session[cas_attributes_session_key])

    return True
